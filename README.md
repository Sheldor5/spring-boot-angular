# Spring Boot + Angular

Backend and Frontend in one Executable.

## Build

````shell script
mvn clean package
````

## Run

````shell script
java -jar backend/target/backend-0.0.1-SNAPSHOT.jar
````

## Test

[Open Browser Link](http://localhost:8080/)
